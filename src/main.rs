#[macro_use]
extern crate clap;
extern crate daemonize;
extern crate ip_api;
#[macro_use]
extern crate lazy_static;
extern crate pledge;
extern crate regex;
#[macro_use]
extern crate slog;
extern crate slog_syslog;

use regex::Regex;
use slog::*;
use slog_syslog::Facility;
use std::convert;
use std::error::{self, Error};
use std::fs;
use std::os::unix::fs::symlink;
use std::fmt;
use std::io;
use std::path;
use std::process;
use std::result;
use std::thread;
use std::time;

#[allow(non_upper_case_globals)]
lazy_static! {
    static ref TZ_REGEX: Regex = Regex::new(r"^[A-Z][A-Za-z0-9+-]+/[A-Za-z0-9+_-]+$").unwrap();
    static ref log: Logger = init_logger();
}

static BASE_PATH: &'static str = "/usr/share/zoneinfo";
static LOCALTIME_PATH: &'static str = "/etc/localtime";

#[derive(Debug)]
enum TimezonedError {
    Unknown,
    IoError(io::Error),
    IpApiError(ip_api::IpApiError),
}

impl convert::From<ip_api::IpApiError> for TimezonedError {
    fn from(e: ip_api::IpApiError) -> Self {
        TimezonedError::IpApiError(e)
    }
}

impl convert::From<io::Error> for TimezonedError {
    fn from(e: io::Error) -> Self {
        TimezonedError::IoError(e)
    }
}

impl error::Error for TimezonedError {
    fn description(&self) -> &str {
        match self {
            &TimezonedError::Unknown => "Could not derive timezone",
            &TimezonedError::IpApiError(_) => "Encountered an error talking to ip-api.com",
            &TimezonedError::IoError(_) => "Encountered an error with the OS",
        }
    }

    fn cause(&self) -> Option<&error::Error> {
        match self {
            &TimezonedError::IpApiError(ref e) => Some(e),
            &TimezonedError::IoError(ref e) => Some(e),
            _ => None,
        }
    }
}

impl fmt::Display for TimezonedError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.cause() {
            Some(e) => write!(f, "{}", e.description()),
            None => write!(f, "{}", self.description()),
        }
    }
}

#[derive(Clone, Copy, Debug)]
struct Config {
    ipapi: bool,
    interval: u64,
    dryrun: bool,
    debug: bool,
    daemonize: bool,
}

fn lookup(cfg: &Config) -> result::Result<String, TimezonedError> {
    if cfg.ipapi {
        let loc = ip_api::GeoIp::new(None, false)?;

        match loc.timezone() {
            Some(l) => Ok(l),
            None => Err(TimezonedError::Unknown),
        }
    } else {
        Err(TimezonedError::Unknown)
    }
}

fn validate<T: AsRef<str>>(tz: T) -> bool
where
    T: AsRef<path::Path>,
{
    if !TZ_REGEX.is_match(tz.as_ref()) {
        warn!(log, "failed to match safety regex");
        return false;
    }

    fs::metadata(path::Path::new(BASE_PATH).join(tz)).is_ok()
}

fn replace<T: AsRef<path::Path>>(tz: T) -> result::Result<(), TimezonedError> {
    fs::remove_file(LOCALTIME_PATH)?;
    symlink(path::Path::new(BASE_PATH).join(tz), LOCALTIME_PATH)?;
    Ok(())
}

fn run(cfg: &Config) -> result::Result<(), TimezonedError> {
    let tz = lookup(&cfg)?;

    if validate(&tz) {
        if cfg.dryrun {
            warn!(log, "would replace /etc/localtime");
        } else {
            debug!(log, "replacing /etc/localtime");
            replace(tz)?;
        }
    }

    Ok(())
}

fn init_logger() -> Logger {
    if let Ok(drain) = slog_syslog::unix_3164(Facility::LOG_USER) {
        Logger::root(drain.fuse(), o!())
    } else {
        Logger::root(Discard, o!())
    }
}

fn main() {
    let matches =
        clap_app!(timezoned =>
                  (version: "0.1")
                  (author: "William Orr <will@worrbase.com>")
                  (about: "Updates /etc/localtime by location")
                  (@arg ipapi: --("ip-api") "Use ip-api.com servers for location information")
                  (@arg interval: --interval <INTERVAL> default_value("30") "Ping server at specified interval")
                  (@arg daemonize: --daemonize "Run as a daemon")
                  (@arg dryrun: --("dry-run") "Do not modify /etc/localtime")
                  (@arg debug: --debug "Turn on debugging information")).get_matches();

    let cfg = Config {
        ipapi: matches.is_present("ipapi"),
        interval: value_t!(matches.value_of("interval"), u64).unwrap_or_else(|e| e.exit()),
        dryrun: matches.is_present("dryrun"),
        debug: matches.is_present("debug"),
        daemonize: matches.is_present("daemonize"),
    };

    if let Err(e) = pledge::pledge("stdio id proc inet cpath rpath wpath dns flock ps unix") {
        crit!(log, "{}", e);
        process::exit(1);
    }

    if cfg.daemonize {
        debug!(log, "Daemonizing...");
        let daemon = daemonize::Daemonize::new()
            .pid_file("/var/run/timezoned.pid")
            .start();

        if let Err(e) = daemon {
            crit!(log, "{}", e);
            process::exit(1);
        }

        if let Err(e) = pledge::pledge("stdio inet cpath rpath dns ps unix") {
            crit!(log, "{}", e);
            process::exit(1);
        }

        let sleep_duration = time::Duration::from_secs(60 * cfg.interval);
        loop {
            if let Err(e) = run(&cfg) {
                crit!(log, "{}", e);
            }

            thread::sleep(sleep_duration);
        }
    } else {
        if let Err(e) = pledge::pledge("stdio inet cpath rpath dns ps unix") {
            crit!(log, "{}", e);
            process::exit(1);
        }

        if let Err(e) = run(&cfg) {
            crit!(log, "{}", e);
            process::exit(1);
        }
    }
}
